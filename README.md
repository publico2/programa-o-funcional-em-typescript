# Programação Funcional em Typescript
**Autor *Rodrigo de França de Sá Menezes***

*Este faz um resumo de como aplicar conceitos do paradigma funcional na linguagem Typescript, que por sua vêz é  considerada uma linguagem multiparadigma, como por exemplo o paradigma estrutural, orientado a objetos, orientado a aspecto e funcional. TypeScript é um super-set da linguagem JavaScript. Portanto implementa todos os recursos já disponíveis em JavaScript. A parte da linguagem envolvendo conceitos funcionais descritas ao decorrer do texto podem ser replicadas em JavaScript de forma análoga, porém sem a notação de tipo.*

## 1 - Introdução

A linguagem TypeScript surgiu em 2012 tem sido cada vez mais usada na comunidade de desenvolvedores. Ela da uma certa liberdade para o programador trabalhar de forma mais ou menos restritiva, dependendo do projeto e do programador. Na programação funcional temos conceitos importantes como *imutabilidade*, *funções como valores de primeira classe*, *funções de alta ordem*, *currying*, *recursividade*, *closure* e *funções puras*, que serão abordados ao longo deste artigo e implementados em Typescript.


## 2 - Imutabilidade
O conceito de imutabilidade de dados nos diz que um dado ao ser criado não pode ser alterado. Terá sempre o mesmo valor. Sabe-se que a base da programação imperativa é a mudança e controle de estado que é representado por uma variável um ou conjunto delas. Em programação funcional o conceito de variável é inexistente. Um indentificador (nome de função, constante, etc) deve sempre ao longo de todo o código representar a mesma coisa.

Em TypeScript pode-se declarar uma variável de 3: usando  *var*, *let* e *const*.

* *var*: declara uma variável em escopo de função. É mutável.
Ex.:
```typescript
    var a: number; //declaração
    a = 5; // atribuição (mutação de dados)

    var b: number = 6; // declaração com a atribuição
    b = 7; //atribuição (mutação de dados)
```
* *let*: declara uma variável em escopo local. É mutável.
Ex.:
```typescript
    var c: string; //declaração
    c = "foo"; // atribuição (mutação de dados)

    var d: string = "car"; // declaração com a atribuição
    d = "bar"; //atribuição (mutação de dados)
```
* *const*: declara uma variável em escopo local. Não é mutável.
Ex.:
```typescript
    const e: number; // ERRO! Não se pode declarar uma constante sem indicar seu valor.

    const pi: number = 3.14; //Correto
    pi = 3.145; // ERRO! Não se pode alterar o valor de uma constante;
```

Portanto, para implementar a imutabilidade em TypeScript deve-se usar a palavra chave *const* para declarar as variáveis.

### 2.1 - Caso especial de objetos e *arrays*

Em TypeScript uma variavel declara como constante não pode sofrer atribuição, porém ela pode sofrer mudanças internas. Em constantes de tipos básicos como *Number*, *Boolean* isso não é relevante, mas no caso de objetos e *array* se torna um problema pois quebra o conceito de imutabilidade.

Ex. para objetos:
```typescript
    interface Pessoa { nome: string, idade: number}
    
    const p1: Pessoa = { nome: "Fulano", idade: 17};
    p1 = {nome: "Erro", idade 17}; // ERRO!

    p1.nome = "Mudou"; //SEM ERRO! Pois tenta mudar o estado de p1

    console.log(p1);
    // saída: { nome: "Mudou", idade: 17}
```

Ex. para *arrays*:

```typescript
    const v1: number[] = [3,2,5];
    v1 = [3,2,5,7]; //SEM ERRO! Pois tenta mudar o estado de v1

    v1.push(7); //SEM ERRO! Muda o estado de p1

    console.log(v1);
    // saída: [3, 2, 5, 7]
```

Para solucionar esse problema, pode-se utilizar o tipo utilitário *Readonly\<T>*.

Ex. para objetos:
```typescript
    interface Pessoa { nome: string, idade: number}
    
    const p1: Readonly<Pessoa> = { nome: "Fulano", idade: 17};
    p1 = {nome: "Erro", idade 17}; // ERRO!

    p1.nome = "Mudou"; //ERRO! Pois as propriedades de p1 são imutáveis
```

Ex. para *arrays*:

```typescript
    const v1: Readonly<number[]> = [3,2,5];
    v1 = [3,2,5,7]; //SEM ERRO! Pois tenta mudar o estado de v1

    v1.push(7); //ERRO! Pois as propriedades de v1 são imutáveis
```

## 3 - Funções como valores de primeira classe

Em linguagens de programação em que as funções são tratadas como valores de primeira classe as funções podem ser literais, atribuidas em variáveis, passadas como parâmetro de função e até mesmo serem usadas como retorno de função. Funções que recebem como parâmetro ou retornam funções são consideradas funções de alta ordem.

Ex. Literais e atruibuição a variáveis:

```typescript
    type TipoFunc = (x: number) => number;

    const exNum: number = 7; //7 é um valor numérico literal
    const exFunc: TipoFunc = x => x*2; //'x => x*2' é um valor literal do tipo Função

    //outros exemplos literais;
    const quadrado: TipoFunc = function(x: number) {
        return x*x;
    };

    const cubo: TipoFunc = (x: number) => {
        return x*x*x;
    };
```

Ex. Passagem como parâmetro:
```typescript
    type FuncOperacao = <T>(v: T[]) => T;

    const inicioDeLista = (v: any[]) => v[0];
    const fimDeLista = (v: any[]) => v[v.length - 1];

    //esta é uma função de alta ordem
    const aplicaOperacao = <T>(op: FuncOperacao, lista: v: T[]): T => {
        return op(lista);
    };

    aplicaOperacao(inicioDeLista, [2,3,4]);
    // 2

    aplicaOperacao(fimDeLista, ['b', 'c', 'd']);
    // 'd'
```

Ex. Como retorno de função:
```typescript
    type Operacao = 'inicio' | 'fim';
    type FnGetOperacao = <T>(op: Operacao) => <T>(v: T[]) => T;


    //esta é uma função de alta ordem
    const getOperacao: FnGetOperacao = (op: Operacao) => {
        if (op === 'inicio') {
            return <T>(v: T[]) => v[0];
        }
        else {
            return <T>(v: T[]) => v[v.length - 1];
        }
    };

    const inicioDeLista = getOperacao('inicio');
    const fimDeLista = getOperacao('fim');

    inicioDeLista([2,3,4]);
    // 2

    fimDeLista(['b', 'c', 'd']);
    // 'd'
```


## 4 - *Currying*

O conceito de *currying* está relacionado à técnica de tranformar uma função de **n** argumentos em uma série de **n** funções com um argumento.

Ex. sem usar *currying*:

```typescript
    const soma: (a: number, b: number) => number = (a,b) => a+b;
    
    soma(1,3); // 4
```

Ex. usando *currying*:

```typescript
    type FuncSoma = (a: number) => (b: number) => number;
    const soma: FuncSoma = a => b => a + b;
    
    const soma3 = soma(3);
    const soma4 = soma(4);

    soma3(5);   // 8
    soma4(5);   // 9
    soma(2)(5); // 7
```

## 5 - Recursividade

Uma função é considerada recursiva quando ela faz, dentro de si, uma ou mais chamadas dela ela mesma. Essas subchamadas também fazem outras subchamadas recursivas até se atingir uma certa condição de parada, interrompendo a recursão.

Ex. iterativo:
```typescript
    const fatorial = (n: number) => {
        let prod: number = 1;
        for (let i = n; i > 1; i++) {
            prod *= i
        }
        return prod;
    }

    fatorial(5); // 120
```

Ex. recursivo:
```typescript
    const fatorial = (n: number) => n <= 1 ? 1 : fatorial(n-1);
    fatorial(5); // 120
```

## 6 - *Closure*

Uma *closure* é uma função que referencia variáveis livres no contexto léxico. Uma *closure* ocorre normalmente quando uma função é declarada dentro do corpo de outra, e a função interior referencia variáveis locais da função exterior.

Ex.:
```typescript
    type FabricFunc = (y: number) => (x: number) => number;

    const FabricaMultiplicador = (y: number) => {
        //nesse caso 'multi' é uma closure
        const multi = (x: number) => x*y;
        return multi;
    };

    const multi5 = FabricaMultiplicador(5);
    const multi6 = FabricaMultiplicador(6);

    multi5(5); // 25
    multi6(5)); // 30
```

## 7 - Funções Puras

Funções puras são funções que o seu valor de retorno depende somente dos valores passados por parâmetro. Ela também não deve alterar nenhum estado fora de seu escopo.

Ex.:
```typescript
    // é pura
    const soma: (a: number, b: number) => number = (a,b) => {
        return a+b;
    }

    // não é pura pois não retorna o mesmo valor sempre que chamada
    const getSegundoAtual: () => number = () => {
        return new Date().getSeconds();
    }


    //não é pura pois modifica algo no contexto fora do seu
    let contadorDeExecuções = 0;
    const multi = (a: number, b: number) => {
        contadorDeExecuções++;
        return a*b;
    }
``` 